const express = require('express');
const app = express();
const mysql = require('mysql');

var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'

var connection = mysql.createConnection({
  host: process.env.SERVER_DATABASE,
  user: process.env.USER_DATABASE,
  password: process.env.PASSWORD_DATABASE,
  database: process.env.NAME_DATABASE
});

connection.connect();

app.get('/rest/archivos', function (req, res) {

   
   connection.query('SELECT * FROM Archivos', function (error, results, fields)  {
	if (error) throw error;
	return res.send({ error: false, data: results, message: 'Todos list'});


});

});



app.listen(server_port,server_ip_address, function () {
console.log( "Listening on " + server_ip_address + ", port " + server_port );

});
